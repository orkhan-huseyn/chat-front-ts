import { SET_THEME } from 'store/definitions'
import { SetThemeAction } from 'store/types'
import { Theme } from 'utils/types'

export const setThemeAction = (theme: Theme): SetThemeAction => {
  return {
    type: SET_THEME,
    payload: theme,
  }
}
