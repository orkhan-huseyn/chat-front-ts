import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import conversationsReducer from './reducers/conversations'
import currentConversationReducer from './reducers/currentConversation'
import currentUserReducer from './reducers/currentUser'
import messagesReducer from './reducers/messages'
import themeReducer from './reducers/theme'

const rootReducer = combineReducers({
  currentUser: currentUserReducer,
  conversations: conversationsReducer,
  messages: messagesReducer,
  currentConversation: currentConversationReducer,
  theme: themeReducer,
})

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
)

export default store
