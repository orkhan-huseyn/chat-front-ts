import { SET_THEME } from 'store/definitions'
import { ThemeAction, ThemeState } from 'store/types'
import { defaultTheme } from 'utils/theme'

export default function themeReducer(
  state = defaultTheme,
  action: ThemeAction
): ThemeState {
  switch (action.type) {
    case SET_THEME:
      const theme = action.payload
      return theme
    default:
      return state
  }
}
