import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Switch, Route, Redirect } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { GlobalStyles } from 'utils/globalStyles'
import { setCurrentUserAction } from 'store/actions/currentUser'
import { setThemeAction } from 'store/actions/theme'
import { darkTheme, defaultTheme } from 'utils/theme'
import { RootState, User } from 'store/types'

import Dashboard from './pages/Dashboard'
import Registration from './pages/Registration'
import Login from './pages/Login'

function App() {
  const dispatch = useDispatch()
  const theme = useSelector((state: RootState) => state.theme)

  const darkModeMathcer = window.matchMedia('(prefers-color-scheme: dark)')
  const accessToken = localStorage.getItem('access_token')

  useEffect(() => {
    function handleSystemColorSchemeChange() {
      if (darkModeMathcer.matches) {
        dispatch(setThemeAction(darkTheme))
      } else {
        dispatch(setThemeAction(defaultTheme))
      }
    }
    darkModeMathcer.addEventListener('change', handleSystemColorSchemeChange)
  }, [darkModeMathcer, dispatch])

  if (accessToken) {
    const user: User = JSON.parse(localStorage.getItem('user_info') as string)
    dispatch(setCurrentUserAction(user))
    return (
      <ThemeProvider theme={theme}>
        <Switch>
          <Route exact path="/" component={Dashboard} />
        </Switch>
        <GlobalStyles />
      </ThemeProvider>
    )
  }

  return (
    <ThemeProvider theme={theme}>
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/registration" component={Registration} />
        <Route render={() => <Redirect to="/login" />} />
      </Switch>
      <GlobalStyles />
    </ThemeProvider>
  )
}

export default App
