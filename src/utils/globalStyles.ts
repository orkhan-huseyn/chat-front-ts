import { createGlobalStyle } from 'styled-components'
import { normalize } from 'polished'

export const GlobalStyles = createGlobalStyle`
    ${normalize()}

    *, *::after, *::before {
        box-sizing: border-box;
    }

    html {
        font-size: 16px;
    }

    body {
        font-family: 'Roboto', sans-serif;
        background-color: ${(props: any) => props.theme.backgroundColor};
        color: ${(props: any) => props.theme.textColor};
    }

    a {
        color: ${(props: any) => props.theme.primaryColor};
        text-decoration: none;
    }

    .mb-10 {
        margin-bottom: 10px !important;
    }

    .mb-15 {
        margin-bottom: 15px !important;
    }

    .mb-20 {
        margin-bottom: 20px !important;
    }
`
