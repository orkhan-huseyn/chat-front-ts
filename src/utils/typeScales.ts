export const typeScales = {
  header1: '1.8rem',
  header2: '1.6rem',
  header3: '1.4rem',
  header4: '1.2rem',
  header5: '1.1rem',
  paragraph: '1rem',
  helper: '0.8rem',
  small: '0.7rem',
}
