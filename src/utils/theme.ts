import { Theme } from 'utils/types'

export const defaultTheme: Theme = {
  dark: false,
  primaryColor: '#0081ff',
  dangerColor: '#dc3545',
  textColor: '#44566c',
  textColorLight: '#8697a8',
  backgroundColor: '#ffffff',
  backgroundColorLight: '#eff4f8',
  textInputBackground: '#f8fafb',
  borderColor: '#eaedf0',
}

export const darkTheme: Theme = {
  dark: true,
  primaryColor: '#0081ff',
  dangerColor: '#dc3545',
  textColor: '#ffffff',
  textColorLight: '#737781',
  backgroundColorLight: '#333e4c',
  backgroundColor: '#1f2935',
  textInputBackground: '#15232e',
  borderColor: '#333e4c',
}
