export interface Theme {
  dark: boolean
  primaryColor: string
  dangerColor: string
  textColor: string
  textColorLight: string
  backgroundColor: string
  backgroundColorLight: string
  textInputBackground: string
  borderColor: string
}
