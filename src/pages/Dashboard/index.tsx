import React from 'react'

const Dashboard = React.lazy(() => import('./Dashboard'))

const LazyDashboard = () => (
  <React.Suspense fallback={<span>Loading...</span>}>
    <Dashboard />
  </React.Suspense>
)

export default LazyDashboard
