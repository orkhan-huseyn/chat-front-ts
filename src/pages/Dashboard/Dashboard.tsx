import { ConversationItem } from 'components/ConversationItem'
import { Navbar } from 'components/Navbar'

function Dashboard() {
  return (
    <>
      <Navbar />
      <h1>Hello from Dashboard page</h1>
      <ConversationItem />
    </>
  )
}

export default Dashboard
