import React from 'react'

const Login = React.lazy(() => import('./Login'))

const LazyLogin = () => (
  <React.Suspense fallback={<span>Loading...</span>}>
    <Login />
  </React.Suspense>
)

export default LazyLogin
