import * as Yup from 'yup'
import styled from 'styled-components'
import { useFormik } from 'formik'
import { Link } from 'react-router-dom'
import { Button } from 'components/Button'
import { TextInput } from 'components/TextInput'
import { HelperText } from 'components/HelperText'
import axios from 'api/axios'

interface Values {
  username: string | undefined
  password: string | undefined
}

const ValidationSchema = Yup.object().shape({
  username: Yup.string().required(),
  password: Yup.string().required(),
})

function Login() {
  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
    },
    validationSchema: ValidationSchema,
    onSubmit: async (values: Values, { setSubmitting }) => {
      const { data } = await axios.post('login', values)
      setSubmitting(false)
      if (!data.error) {
        localStorage.setItem('access_token', data.payload.accessToken)
        localStorage.setItem('user_info', JSON.stringify(data.payload.user))
        window.location.href = '/'
      }
    },
  })

  return (
    <Container>
      <LoginForm onSubmit={formik.handleSubmit} autoComplete="off">
        <h1>Log in</h1>
        <TextInput
          error={Boolean(formik.errors.username && formik.touched.username)}
          type="text"
          name="username"
          placeholder="Enter your username"
          aria-label="Enter your username"
          value={formik.values.username}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        />
        {Boolean(formik.errors.username && formik.touched.username) ? (
          <HelperText error>You must enter your username</HelperText>
        ) : null}
        <TextInput
          error={Boolean(formik.errors.password && formik.touched.password)}
          type="password"
          name="password"
          placeholder="Enter your password"
          aria-label="Enter your password"
          value={formik.values.password}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        />
        {Boolean(formik.errors.password && formik.touched.password) ? (
          <HelperText error>You must enter your password</HelperText>
        ) : null}
        <Button
          type="submit"
          disabled={formik.isSubmitting || !formik.isValid}
          block
        >
          {formik.isSubmitting ? 'Joininig chat...' : 'Join chat'}
        </Button>
        <Link to="/registration">Don't have an account?</Link>
      </LoginForm>
    </Container>
  )
}

const Container = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`

const LoginForm = styled.form`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 400px;
  padding: 25px;
  background-color: ${(props) => props.theme.backgroundColor};
  border-radius: 10px;
  ${(props) =>
    props.theme.dark &&
    `filter: drop-shadow(0px 2px 4px rgba(0, 0, 0, 0.05)) drop-shadow(0px 12px 16px rgba(0, 0, 0, 0.2));`}

  ${(props) =>
    !props.theme.dark &&
    `filter: drop-shadow(0px 2px 4px rgba(169, 194, 209, 0.05))
      drop-shadow(0px 12px 16px rgba(169, 194, 209, 0.1));`}
`

export default Login
