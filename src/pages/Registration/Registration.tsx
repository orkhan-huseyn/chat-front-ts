import { useState } from 'react'
import styled from 'styled-components'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import { Link, useHistory } from 'react-router-dom'
import { Button } from 'components/Button'
import { TextInput } from 'components/TextInput'
import { HelperText } from 'components/HelperText'
import axios from 'api/axios'

interface Values {
  fullName: string | undefined
  username: string | undefined
  position: string | undefined
  password: string | undefined
}

const ValidationSchema = Yup.object().shape({
  fullName: Yup.string().required(),
  username: Yup.string().required(),
  position: Yup.string().required(),
  password: Yup.string().required(),
})

function Registration() {
  const history = useHistory()
  const [redirecting, setRedirecting] = useState(false)

  const formik = useFormik({
    initialValues: {
      fullName: '',
      username: '',
      position: '',
      password: '',
    },
    validationSchema: ValidationSchema,
    onSubmit: async (values: Values, { setSubmitting }) => {
      await axios.post('registration', values)
      setRedirecting(true)
      setSubmitting(false)
      setTimeout(() => {
        setRedirecting(false)
        history.push('/login')
      }, 2000)
    },
  })

  return (
    <Container>
      <RegistrationForm onSubmit={formik.handleSubmit} autoComplete="off">
        <h1>Register</h1>
        {redirecting ? (
          <HelperText className="mb-20" center>
            Redirecting to login page...
          </HelperText>
        ) : null}
        <TextInput
          error={Boolean(formik.errors.fullName && formik.touched.fullName)}
          type="text"
          name="fullName"
          placeholder="Enter your full name"
          aria-label="Enter your full name"
          value={formik.values.fullName}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        />
        {Boolean(formik.errors.fullName && formik.touched.fullName) ? (
          <HelperText error>You must enter your full name</HelperText>
        ) : null}

        <TextInput
          error={Boolean(formik.errors.username && formik.touched.username)}
          type="text"
          name="username"
          placeholder="Enter your username"
          aria-label="Enter your username"
          value={formik.values.username}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        />
        {Boolean(formik.errors.username && formik.touched.username) ? (
          <HelperText error>You must enter your username</HelperText>
        ) : null}

        <TextInput
          error={Boolean(formik.errors.position && formik.touched.position)}
          type="text"
          name="position"
          placeholder="Enter your position"
          aria-label="Enter your position"
          value={formik.values.position}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        />
        {Boolean(formik.errors.position && formik.touched.position) ? (
          <HelperText error>You must enter your position</HelperText>
        ) : null}

        <TextInput
          error={Boolean(formik.errors.password && formik.touched.password)}
          type="password"
          name="password"
          placeholder="Enter your password"
          aria-label="Enter your password"
          value={formik.values.password}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        />
        {Boolean(formik.errors.password && formik.touched.password) ? (
          <HelperText error>You must enter your password</HelperText>
        ) : null}

        <Button
          type="submit"
          disabled={formik.isSubmitting || !formik.isValid || redirecting}
          block
        >
          {formik.isSubmitting ? 'Sending...' : 'Register'}
        </Button>
        <Link to="/login">Already have an account?</Link>
      </RegistrationForm>
    </Container>
  )
}

const Container = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`

const RegistrationForm = styled.form`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 400px;
  padding: 25px;
  background-color: ${(props) => props.theme.backgroundColor};
  border-radius: 10px;
  ${(props) =>
    props.theme.dark &&
    `filter: drop-shadow(0px 2px 4px rgba(0, 0, 0, 0.05)) drop-shadow(0px 12px 16px rgba(0, 0, 0, 0.2));`}

  ${(props) =>
    !props.theme.dark &&
    `filter: drop-shadow(0px 2px 4px rgba(169, 194, 209, 0.05))
      drop-shadow(0px 12px 16px rgba(169, 194, 209, 0.1));`}
`

export default Registration
