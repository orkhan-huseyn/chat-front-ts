import React from 'react'

const Registration = React.lazy(() => import('./Registration'))

const LazyRegistration = () => (
  <React.Suspense fallback={<span>Loading...</span>}>
    <Registration />
  </React.Suspense>
)

export default LazyRegistration
