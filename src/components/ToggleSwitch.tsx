import { KeyboardEvent, useState } from 'react'
import styled from 'styled-components'

interface ToggleSwitchProps {
  id: string
  labelText: string
  defaultChecked?: boolean
  onChange?: (checked: boolean) => void
}

export const ToggleSwitch = ({
  id,
  labelText,
  defaultChecked = false,
  onChange,
}: ToggleSwitchProps) => {
  const [checked, setChecked] = useState(defaultChecked)

  const handleChange = (event: any) => {
    setChecked(event.target.checked)
    if (typeof onChange === 'function') {
      onChange(!checked)
    }
  }

  const handleKeyPress = (event: KeyboardEvent<any>) => {
    if (event.key === ' ') {
      setChecked(!checked)
      if (typeof onChange === 'function') {
        onChange(!checked)
      }
    }
  }

  return (
    <>
      <StyledCheckbox
        id={id}
        type="checkbox"
        aria-label={labelText}
        checked={checked}
        onChange={handleChange}
      />
      <StyledLabel onKeyPress={handleKeyPress} tabIndex={0} htmlFor={id} />
    </>
  )
}

const StyledCheckbox = styled.input`
  display: none;

  &:checked ~ label {
    background-color: ${(props) => props.theme.primaryColor};
  }

  &:checked ~ label::after {
    transform: translateX(155%);
  }
`

const StyledLabel = styled.label`
  transition: background-color 0.1s linear;
  cursor: pointer;
  position: relative;
  background-color: ${(props) => props.theme.backgroundColorLight};
  border-radius: 50px;
  height: 30px;
  width: 60px;

  &::after {
    transition: transform 0.1s linear;
    content: '';
    position: absolute;
    left: 5px;
    top: 5px;
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: #ffffff;
    box-shadow: 0px 2px 2px rgba(68, 86, 108, 0.2);
  }
`
