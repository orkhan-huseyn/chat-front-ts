import styled from 'styled-components'
import { typeScales } from 'utils/typeScales'

interface BadgeProps {
  color?: string
  block?: boolean
}

const defaultColor = '#ff3d57'

export const Badge = styled.span<BadgeProps>`
  display: ${(props) => (props.block ? 'flex' : 'inline-flex')};
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.color || defaultColor};
  width: 20px;
  height: 20px;
  color: #ffffff;
  border-radius: 50%;
  font-size: ${typeScales.small};
`
