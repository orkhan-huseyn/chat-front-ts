import styled from 'styled-components'
import { Avatar } from 'components/Avatar'
import { Badge } from 'components/Badge'
import { typeScales } from 'utils/typeScales'

export const ConversationItem = () => {
  return (
    <Container>
      <AvatarWrapper online>
        <Avatar>FB</Avatar>
      </AvatarWrapper>
      <ConversationInfo>
        <FlexHelper>
          <StyledHeading>Felecia Burke</StyledHeading>
          <StyledParagraph>The Arts play a large role in...</StyledParagraph>
        </FlexHelper>
        <FlexHelper>
          <StyledHelperText>1 min</StyledHelperText>
          <Badge>1</Badge>
        </FlexHelper>
      </ConversationInfo>
    </Container>
  )
}

const Container = styled.button`
  border: none;
  cursor: pointer;
  display: flex;
  width: 100%;
  align-items: center;
  background-color: rgba(196, 196, 196, 0.1);
  border-top: 1px solid ${(props) => props.theme.borderColor};
  border-bottom: 1px solid ${(props) => props.theme.borderColor};
  padding: 10px;
  color: ${(props) => props.theme.textColor};
`

const FlexHelper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

const ConversationInfo = styled.div`
  padding-left: 10px;
  display: flex;
  justify-content: space-between;
  width: 100%;
`

const AvatarWrapper = styled.div<any>`
  position: relative;
  display: inline-block;

  &::after {
    display: none;
    content: '';
    position: absolute;
    width: 9px;
    height: 9px;
    background: #09b66d;
    border: 2px solid #ffffff;
    border-radius: 50%;
    bottom: 1px;
    right: 1px;
  }

  ${(props) => props.online && `&::after { display: block; }`}
`

const StyledParagraph = styled.p`
  align-self: flex-start;
  margin: 0;
  font-size: ${typeScales.paragraph};
  color: ${(props) => props.theme.textColorLight};
`

const StyledHeading = styled.h5`
  align-self: flex-start;
  margin: 0 0 5px 0;
  font-size: ${typeScales.header5};
`

const StyledHelperText = styled.span`
  margin: 0 0 5px 0;
  font-size: ${typeScales.helper};
  color: ${(props) => props.theme.textColorLight};
`
