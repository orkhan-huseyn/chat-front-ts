import styled from 'styled-components'
import { typeScales } from 'utils/typeScales'

interface HelperTextProps {
  error?: boolean
  center?: boolean
}

export const HelperText = styled.small<HelperTextProps>`
  display: block;
  align-self: flex-start;
  text-align: left;
  margin: 5px 0;
  font-size: ${typeScales.small};

  ${(props) => props.error && `color: ${props.theme.dangerColor};`}

  ${(props) => props.center && `text-align: center; align-self: center;`}
`
