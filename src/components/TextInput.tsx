import styled from 'styled-components'
import { typeScales } from 'utils/typeScales'

interface TextInputProps {
  error?: boolean
}

export const TextInput = styled.input<TextInputProps>`
  display: block;
  width: 100%;
  background-color: ${(props) => props.theme.textInputBackground};
  color: ${(props) => props.theme.textColor};
  border: 2px solid transparent;
  border-radius: 5px;
  padding: 10px 15px;
  font-family: 'Roboto', sans-serif;
  font-size: ${typeScales.paragraph};
  font-weight: 400;
  margin-bottom: 15px;

  ${(props) =>
    props.error &&
    `border-color: ${props.theme.dangerColor}; margin-bottom: 0;`}
`
