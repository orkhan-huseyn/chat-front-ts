import { cloneElement, ReactElement, useEffect, useRef, useState } from 'react'
import { useSpring, animated } from 'react-spring'
import styled from 'styled-components'

interface DropdownProps {
  trigger: ReactElement
  children: ReactElement
}

export const Dropdown = ({ trigger, children }: DropdownProps) => {
  const dropdownRef = useRef(null)
  const [open, setOpen] = useState(false)
  const animation = useSpring({
    opacity: open ? 1 : 0,
  })

  useEffect(() => {
    const handleOutsideClick = (event: MouseEvent) => {
      if (dropdownRef && dropdownRef.current) {
        const dropdown = dropdownRef.current as HTMLElement
        const clickedDomElement = event.target as HTMLElement

        if (!dropdown.contains(clickedDomElement)) {
          setOpen(false)
        }
      }
    }

    document.addEventListener('click', handleOutsideClick)
    return () => {
      document.removeEventListener('click', handleOutsideClick)
    }
  }, [])

  const handleTriggerClick = () => {
    setOpen(!open)
  }

  const modifiedTrigger = cloneElement(trigger, { onClick: handleTriggerClick })

  return (
    <DropdownContainer ref={dropdownRef}>
      {modifiedTrigger}
      <DropdownContent style={animation}>{children}</DropdownContent>
    </DropdownContainer>
  )
}

const DropdownContainer = styled.div`
  position: relative;
`

const DropdownContent = styled(animated.div)<any>`
  position: absolute;
  top: 45px;
  width: 100%;
  border: 1px solid ${(props) => props.theme.borderColor};
  border-radius: 5px;
  background-color: ${(props) => props.theme.backgroundColor};
  padding: 8px 0;

  &::before {
    content: '';
    position: absolute;
    bottom: 100%;
    right: 5%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-radius: 1px;
    border-color: transparent transparent ${(props) => props.theme.borderColor}
      transparent;
  }

  &::after {
    content: '';
    position: absolute;
    bottom: 100%;
    right: 6%;
    margin-left: -5px;
    border-width: 3px;
    border-style: solid;
    border-radius: 1px;
    border-color: transparent transparent
      ${(props) => props.theme.backgroundColor} transparent;
  }
`
