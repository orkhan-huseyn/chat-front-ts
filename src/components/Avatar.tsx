import styled from 'styled-components'
import { typeScales } from 'utils/typeScales'

const possibleColors = ['#ff8a48', '#ff3d57', '#22cce2', '#ff8a48', '#22cce2']
const randomIndex = Math.floor(Math.random() * possibleColors.length)
const randomColor = possibleColors[randomIndex]

export const Avatar = styled.span`
  display: inline-block;
  font-size: ${typeScales.paragraph};
  font-weight: 500;
  width: 40px;
  height: 40px;
  line-height: 40px;
  border-radius: 50%;
  background-color: ${randomColor};
  color: #ffffff;
  text-align: center;
  vertical-align: middle;
`
