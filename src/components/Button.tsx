import styled from 'styled-components'
import { typeScales } from 'utils/typeScales'

interface ButtonProps {
  block?: boolean
}

export const Button = styled.button<ButtonProps>`
  display: inline-block;
  background-color: ${(props) => props.theme.primaryColor};
  color: #ffffff;
  cursor: pointer;
  border: none;
  border-radius: 5px;
  padding: 10px 25px;
  font-family: 'Roboto', sans-serif;
  font-size: ${typeScales.paragraph};
  font-weight: 500;
  margin-bottom: 20px;

  ${(props) => props.block && `display: block; width: 100%;`}

  ${(props) =>
    props.disabled &&
    `cursor: not-allowed; background-color: rgba(0, 0, 0, 0.3);`}
`
