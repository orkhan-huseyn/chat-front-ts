import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { setThemeAction } from 'store/actions/theme'
import { darkTheme, defaultTheme } from 'utils/theme'

import { Avatar } from 'components/Avatar'
import { Dropdown } from 'components/Dropdown'
import { ToggleSwitch } from 'components/ToggleSwitch'

export const Navbar = () => {
  const dispatch = useDispatch()

  const handleThemeCahnge = (checked: boolean) => {
    if (checked) {
      dispatch(setThemeAction(darkTheme))
    } else {
      dispatch(setThemeAction(defaultTheme))
    }
  }

  return (
    <NavbarContainer>
      <ToggleSwitch
        id="themeSwitch"
        labelText="Switch theme"
        onChange={handleThemeCahnge}
      />
      <Dropdown
        trigger={
          <TransparentButton>
            <Avatar>MA</Avatar>
            <SytledSpan>Mark Anderson</SytledSpan>
            <svg width="8" height="5" viewBox="0 0 8 5" fill="none">
              <path d="M0 0H8L4 5L0 0Z" fill="#737781" />
            </svg>
          </TransparentButton>
        }
      >
        <StyledList>
          <li>
            <StyledLink as={Link} to="/profile">
              Profile
            </StyledLink>
          </li>
          <li>
            <StyledLink as={Link} to="/logout">
              Logout
            </StyledLink>
          </li>
        </StyledList>
      </Dropdown>
    </NavbarContainer>
  )
}

const NavbarContainer = styled.header`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  background-color: ${(props) => props.theme.backgroundColor};
  color: ${(props) => props.theme.textColor};
  border: 1px solid ${(props) => props.theme.borderColor};
  height: 64px;
  padding: 0 10px;
`

const TransparentButton = styled.button`
  background-color: transparent;
  border: none;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`
const SytledSpan = styled.span`
  display: inline-block;
  margin: 0 10px;
  color: ${(props) => props.theme.textColor};
`

const StyledList = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
`

const StyledLink = styled.a`
  display: block;
  widht: 100%;
  padding: 10px;
  color: ${(props) => props.theme.textColor};
`
